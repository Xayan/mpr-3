package permissions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import permissions.db.PagingInfo;
import permissions.db.RepositoryCatalog;
import permissions.db.catalogs.HsqlRepositoryCatalog;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	String url = "jdbc:hsqldb:hsql://localhost/workdb";
		try(Connection connection = DriverManager.getConnection(url)) 
		{
	    	RepositoryCatalog catalogOf = new HsqlRepositoryCatalog(connection);  	
	    	catalogOf.people().withName("jan", new PagingInfo());
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}

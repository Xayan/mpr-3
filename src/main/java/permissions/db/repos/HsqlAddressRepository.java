package permissions.db.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.db.PagingInfo;
import permissions.db.AddressRepository;
import permissions.domain.Address;

public class HsqlAddressRepository implements AddressRepository {

	private Connection connection;
	
	private String insertSql ="INSERT INTO address(country,city,street,postal_code,house_number,local_number) VALUES(?,?)"; 
	private String selectSql ="SELECT * FROM address LIMIT ?,?";
	private String selectByIdSql ="SELECT * FROM address WHERE id=?";
	private String selectByCountrySql ="SELECT * FROM address WHERE country=? LIMIT ?,?";
	private String selectByCitySql ="SELECT * FROM address WHERE city=? LIMIT ?,?";
	private String selectByStreetSql ="SELECT * FROM address WHERE street=? LIMIT ?,?";
	private String selectByPostalCodeSql ="SELECT * FROM address WHERE postal_code=? LIMIT ?,?";
	private String selectByHouseNumberSql ="SELECT * FROM address WHERE house_number=? LIMIT ?,?";
	private String selectByLocalNumberSql ="SELECT * FROM address WHERE local_number=? LIMIT ?,?";
	private String deleteSql = "DELETE FROM address WHERE id=?";
	private String updateSql = "UPDATE address SET (country, city, street, postal_code, house_number, local_number)=(?,?,?,?,?,?) WHERE id=?";
	
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectById;
	private PreparedStatement selectByCountry;
	private PreparedStatement selectByCity;
	private PreparedStatement selectByStreet;
	private PreparedStatement selectByPostalCode;
	private PreparedStatement selectByHouseNumber;
	private PreparedStatement selectByLocalNumber;
	private PreparedStatement delete;
	private PreparedStatement update;
	

	private String createAddressTable =""
			+ "CREATE TABLE Person("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "country VARCHAR(50),"
			+ "city VARCHAR(50),"
			+ "street VARCHAR(50),"
			+ "postal_code VARCHAR(6),"
			+ "house_number VARCHAR(5),"
			+ "local_number VARCHAR(5)"
			+ ")";
	
	
	public HsqlAddressRepository(Connection connection) {
		this.connection = connection;
		
		try{
			ResultSet rs = this.connection.getMetaData().getTables(null, null, null, null);
			
			boolean tableExists =false;
			while(rs.next()) {
				if(rs.getString("TABLE_NAME").equalsIgnoreCase("Address")) {
					tableExists=true;
					break;
				}
			}
			if(!tableExists) {
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(createAddressTable);
			}

			insert = connection.prepareStatement(insertSql);
			select = connection.prepareStatement(selectSql);
			selectById = connection.prepareStatement(selectByIdSql);
			selectByCountry = connection.prepareStatement(selectByCountrySql);
			selectByCity = connection.prepareStatement(selectByCitySql);
			selectByStreet = connection.prepareStatement(selectByStreetSql);
			selectByPostalCode = connection.prepareStatement(selectByPostalCodeSql);
			selectByHouseNumber = connection.prepareStatement(selectByHouseNumberSql);
			selectByLocalNumber = connection.prepareStatement(selectByLocalNumberSql);
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(updateSql);
		} catch(SQLException ex){
			ex.printStackTrace();
		}
	}

	@Override
	public Address withId(int id) {
		Address result = null;
		try {
			selectById.setInt(1, id);
			ResultSet rs = selectById.executeQuery();
			while(rs.next()){
				result = new Address();
				result.setCity(rs.getString("city"));
				result.setCountry(rs.getString("country"));
				result.setStreet(rs.getString("street"));
				result.setPostalCode(rs.getString("postal_code"));
				result.setHouseNumber(rs.getString("house_number"));
				result.setLocalNumber(rs.getString("local_number"));
				result.setId(rs.getInt("id"));
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> allOnPage(PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		
		try {
			select.setInt(1, page.getCurrentPage()*page.getSize());
			select.setInt(2, page.getSize());
			ResultSet rs = select.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void add(Address address) {
		try {
			insert.setString(1, address.getCountry());
			insert.setString(2, address.getCity());
			insert.setString(3, address.getStreet());
			insert.setString(4, address.getPostalCode());
			insert.setString(5, address.getHouseNumber());
			insert.setString(6, address.getLocalNumber());
			
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void modify(Address address) {
		try {
			update.setString(1, address.getCountry());
			update.setString(2, address.getCity());
			update.setString(3, address.getStreet());
			update.setString(4, address.getPostalCode());
			update.setString(5, address.getHouseNumber());
			update.setString(6, address.getLocalNumber());
			update.setInt(7, address.getId());
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void remove(Address address) {
		try {
			delete.setInt(1, address.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Address> withCountry(String country, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByCountry.setString(1, country);
			selectByCountry.setInt(2, page.getCurrentPage()*page.getSize());
			selectByCountry.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> withCity(String city, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByCity.setString(1, city);
			selectByCity.setInt(2, page.getCurrentPage()*page.getSize());
			selectByCity.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> withStreet(String street, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByStreet.setString(1, street);
			selectByStreet.setInt(2, page.getCurrentPage()*page.getSize());
			selectByStreet.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> withPostalCode(String postalCode, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByPostalCode.setString(1, postalCode);
			selectByPostalCode.setInt(2, page.getCurrentPage()*page.getSize());
			selectByPostalCode.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> withHouseNumber(String houseNumber, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByHouseNumber.setString(1, houseNumber);
			selectByHouseNumber.setInt(2, page.getCurrentPage()*page.getSize());
			selectByHouseNumber.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Address> withLocalNumber(String localNumber, PagingInfo page) {
		List<Address> result = new ArrayList<Address>();
		try {
			selectByLocalNumber.setString(1, localNumber);
			selectByCity.setInt(2, page.getCurrentPage()*page.getSize());
			selectByCity.setInt(3, page.getSize());
			ResultSet rs = selectByCountry.executeQuery();
			while(rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				address.setStreet(rs.getString("street"));
				address.setPostalCode(rs.getString("postal_code"));
				address.setHouseNumber(rs.getString("house_number"));
				address.setLocalNumber(rs.getString("local_number"));
				address.setId(rs.getInt("id"));
				result.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}

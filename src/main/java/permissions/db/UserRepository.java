package permissions.db;

import java.util.List;

import permissions.domain.User;

public interface UserRepository extends Repository<User> {
	public List<User> withUsername(String country, PagingInfo page);
	public List<User> withPassword(String city, PagingInfo page);
}
